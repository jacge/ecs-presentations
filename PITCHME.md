# Gitpitch test drive

Just checking the toolchain

---

## GitLab

Can I get this to work on GitLab?

---

# Yes!

It does.

Now, what does the heading level mean?

The size of the heading, apparently.

Logical.

---

# Code

---

## Code block slide


    def fib(n):
        if n < 2:
            return 1
         return fib(n-1) + fib(n-2)
@[3](the base case)
@[1,4](recursive call)
